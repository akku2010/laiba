import { Component } from "@angular/core";
import { IonicPage, NavParams, ViewController, ToastController, NavController } from "ionic-angular";

@IonicPage()
@Component({
    selector: 'page-expired',
    templateUrl: './expired.html'
})

export class ExpiredPage {
    data: any;
    isPayNow:any= false;
    constructor(
        public params: NavParams,
        public viewCtrl: ViewController,
        public toastCtrl: ToastController,
        public navCtrl: NavController
    ) {
        if (params.get("param")) {
            this.data = params.get("param");
        }
    }

    activate() {
        // this.navCtrl.push("AddDevicesPage");
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": this.data
        })
        console.log("Redirect to paytm page")
    }
    // activateVehicles(){
    //     console.log("multipalSelects---->yhfhh");
    //     if(this.isPayAlert==false){
    //       let alert = this.alertCtrl.create({
    //         message: "Please Select Vehicle",
    //         buttons: [
    //           {
    //             text: "ok",
    //             handler: () => {
    //               this.ionViewDidEnter();
    //               (err) => {
    //                 console.log(err);
    //               };
    //             },
    //           },
    //         ],
    //       });
    //       alert.present();
    //     }
    //     //
    
    //     if(this.isPayAlert){
    //     if(this.resp_data.paymentgateway == true && this.isPaymentPage.length > 0){
    //     this.navCtrl.push("PaytmwalletloginPage",{
    //       'param':this.selectedArray});
    //     }
    //     else{
    //       this.navCtrl.push(PaymantgatweyPage);
    //     }
    
    //   }
    
    //   }
    dismiss() {
        this.viewCtrl.dismiss();
    }


}