import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
var iabRef = null

@IonicPage()
@Component({
  selector: 'page-tt',
  templateUrl: 'tt.html',
})
export class TtPage {
  data: {};
  resp: any;
  islogin: any;
  iabRef = null;

  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public apiservice: ApiServiceProvider, private iab: InAppBrowser, public alerCtrl: AlertController,
    private toastCtrl: ToastController,
    private themeableBrowser: ThemeableBrowser) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin devices => " + JSON.stringify(this.islogin));
    this.payment();

  }

  insertMyHeader() {
    iabRef.executeScript({
      code: "var b=document.querySelector('body'); var a=document.createElement('div');document.createTextNode('my own header!'); a.appendChild(newContent);b.parentNode.insertBefore(a,b);"
    }, function () {
      alert("header successfully added");
    });
  }

  iabClose(event) {
    //iabRef.removeEventListener('loadstop', replaceHeaderImage);
    iabRef.removeEventListener('exit', this.iabClose);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TtPage');
  }

  call() {
    //window.open(this.resp);
    if (this.resp == undefined) {
      let alerttemp = this.alerCtrl.create({
        message: "invalid-args",
        buttons: [{
          text: 'Okay',
          // handler: () => {
          //   this.navCtrl.setRoot("DashboardPage");
          // }
        }]
      });
      alerttemp.present();
    } else {

      //   const options : InAppBrowserOptions = {
      //     zoom: 'no'
      //   }
      //   const browser = this.iab.create(this.resp,'_self',options);
      // //   // browser.insertCSS()
      //   browser.on('loadstop').subscribe(event => {
      //     browser.insertCSS({ code: "body{color: red;" });
      //  });

      const options: ThemeableBrowserOptions = {
        statusbar: {
          color: '#f54278'
        },
        toolbar: {
          height: 50,
          color: '#d80622'
        },
        title: {
          color: '#ffffff',
          showPageTitle: true
        },
        backButton: {
          wwwImage: '/assets/icon/arrow.png',
          imagePressed: 'back_pressed',
          align: 'left',
          // event: 'backPressed'
          event: 'closePressed'
        },
       
        // closeButton: {
        //   image: 'close',
        //   imagePressed: 'close_pressed',
        //   align: 'right',
        //   event: 'closePressed'
        // },
        
        backButtonCanClose: true
      };

      const browser: ThemeableBrowserObject = this.themeableBrowser.create(this.resp, '_self', options);

      //  this.iab.create(this.resp, '_self', 'location=yes').insertCSS({ code: "body{color: red;" })
      //iabRef = this.iab.create(this.resp, '_blank', 'location=yes');
      //iabRef.addEventListener('loadstop', this.insertMyHeader);
      //iabRef.addEventListener('exit', this.iabClose);
    }

    //browser.on('')
  }

  payment() {
    console.log("inside the payment");
    var url = "https://socket.oneqlik.in/pullData/getSetuLink?mobileNumber=" + this.islogin.phn;
    //var url = "https://socket.oneqlik.in/pullData/getSetuLink?mobileNumber=" + "9481773053";
    this.apiservice.startLoading().present();
    this.apiservice.getdevicesForAllVehiclesApi(url)
      .subscribe(resp => {
        this.apiservice.stopLoading();
        console.log("server image url=> ", resp);
        console.log("chk response", resp);
        this.resp = resp.link
        console.log("final link", this.resp);
        if (this.resp == undefined) {
          let alerttemp = this.alerCtrl.create({
            message: "invalid-args",
            buttons: [{
              text: 'Okay',
              // handler: () => {
              //   this.navCtrl.setRoot("DashboardPage");
              // }
            }]
          });
          alerttemp.present();
        }
        //this.iab.create(this.resp, '_self' );
      },
        (err) => {
          this.apiservice.stopLoading();
        });
    // this.data = {

    //   "mobileNumber":"9481773053"

    // };

    this.apiservice.payment(this.data)
    // .subscribe(response => {
    //   console.log("chk response", response);
    //   this.resp = response.link
    //   console.log("final link", this.resp);
    //   //window.open(this.resp)
    // }
    // )
  }

}
