import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatPage } from './chat';
import { RelativeTime } from '../../pipes/relative-time';
@NgModule({
  declarations: [
    ChatPage,
    RelativeTime
  ],
  imports: [
    IonicPageModule.forChild(ChatPage),
  ],
})
export class ChatPageModule {}
