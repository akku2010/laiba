import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReportsCollectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reports-collection',
  templateUrl: 'reports-collection.html',
})
export class ReportsCollectionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReportsCollectionPage');
  }

  acReport() {
    this.navCtrl.push("AcReportPage")
  }

  dailyReport() {
    this.navCtrl.push("DailyReportPage")
  }

  disance() {
    this.navCtrl.push("DistanceReportPage")
  }

  geo() {
    this.navCtrl.push("GeofenceReportPage")
  }

  iginition() {
    this.navCtrl.push("IgnitionReportPage")
  }

  stoppage() {
    this.navCtrl.push("StoppagesRepoPage")
  }

  trip() {
    this.navCtrl.push("TripReportPage")
  }

  working() {
    this.navCtrl.push("WorkingHoursReportPage")
  }
}
