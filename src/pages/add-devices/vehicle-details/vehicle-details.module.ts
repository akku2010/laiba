import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehicleDetailsPage } from './vehicle-details';
import { IonBottomDrawerModule } from 'ion-bottom-drawer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    VehicleDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(VehicleDetailsPage),
    TranslateModule.forChild(),
    IonBottomDrawerModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class VehicleDetailsPageModule {}
