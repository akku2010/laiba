import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDevicesPage } from './add-devices';
import { SMS } from '@ionic-native/sms';
import { IonBottomDrawerModule } from '../../../node_modules/ion-bottom-drawer';
// import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';
import { ProgressBarModule } from './progress-bar/progress-bar.module';
// import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ModalModule } from 'ngx-bootstrap/modal';
// import { ProgressBarComponent } from './progress-bar/progress-bar';


@NgModule({
  declarations: [
    AddDevicesPage,
    OnCreate,
    // ProgressBarComponent

  ],
  imports: [
    IonicPageModule.forChild(AddDevicesPage),
    TranslateModule.forChild(),
    IonBottomDrawerModule,
    ProgressBarModule,
    // NgbModule,
    ModalModule.forRoot()
  ],
  exports: [
    OnCreate,
    // ProgressBarComponent
  ],
  providers: [
    SMS,
  ]
})
export class AddDevicesPageModule {}
