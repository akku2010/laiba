webpackJsonp([41],{

/***/ 632:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpiredPageModule", function() { return ExpiredPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expired__ = __webpack_require__(749);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ExpiredPageModule = /** @class */ (function () {
    function ExpiredPageModule() {
    }
    ExpiredPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expired__["a" /* ExpiredPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__expired__["a" /* ExpiredPage */]),
                __WEBPACK_IMPORTED_MODULE_3__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            providers: [
            //  {provide: String, useValue: "SoapService"}
            // SoapService
            ],
        })
    ], ExpiredPageModule);
    return ExpiredPageModule;
}());

//# sourceMappingURL=expired.module.js.map

/***/ }),

/***/ 749:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpiredPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExpiredPage = /** @class */ (function () {
    function ExpiredPage(params, viewCtrl, toastCtrl, navCtrl) {
        this.params = params;
        this.viewCtrl = viewCtrl;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.isPayNow = false;
        if (params.get("param")) {
            this.data = params.get("param");
        }
    }
    ExpiredPage.prototype.activate = function () {
        // this.navCtrl.push("AddDevicesPage");
        this.navCtrl.push("PaytmwalletloginPage", {
            "param": this.data
        });
        console.log("Redirect to paytm page");
    };
    // activateVehicles(){
    //     console.log("multipalSelects---->yhfhh");
    //     if(this.isPayAlert==false){
    //       let alert = this.alertCtrl.create({
    //         message: "Please Select Vehicle",
    //         buttons: [
    //           {
    //             text: "ok",
    //             handler: () => {
    //               this.ionViewDidEnter();
    //               (err) => {
    //                 console.log(err);
    //               };
    //             },
    //           },
    //         ],
    //       });
    //       alert.present();
    //     }
    //     //
    //     if(this.isPayAlert){
    //     if(this.resp_data.paymentgateway == true && this.isPaymentPage.length > 0){
    //     this.navCtrl.push("PaytmwalletloginPage",{
    //       'param':this.selectedArray});
    //     }
    //     else{
    //       this.navCtrl.push(PaymantgatweyPage);
    //     }
    //   }
    //   }
    ExpiredPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ExpiredPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-expired',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/laiba/src/pages/live/expired/expired.html"*/'<ion-content padding class="guide-modal">\n\n    <ion-icon class="close-button" id="close-button" name="close-circle" (tap)="dismiss()"></ion-icon>\n    <!-- <div *ngIf="isPayNow " style="display: flex;justify-content: space-around;">\n        <button ion-button  (click)="activateVehicles()" style="background-color: black;color: white;padding: 10px 20px;margin: 10px;font-size: 18px;border-radius: 25px;">\n        <span>Pay Now</span>\n        </button>\n        <button ion-button (click)="supportCall()" style="background-color: black;color: white;padding: 10px 20px;margin: 10px;font-size: 18px;border-radius: 25px;">\n         <ion-icon ios="ios-call" md="md-call" style="color: white;"></ion-icon>Support\n       </button>\n      </div>\n      <div *ngIf="d.expiration_date < now" style="position: relative">\n        <div (click)="activateVehicle(d)">\n          <ion-item style="background-color: black">\n\n            <ion-avatar item-start *ngIf="iconCheck(d.status,d.iconType)" (click)="selected($event,d)">\n              <img *ngIf="d.iconType === \'jjcb\'" width=\'30px\' style="margin-left: 10px;" src={{devIcon}}>\n              <img *ngIf="!isSelected(d)" width=\'20px\' style="margin-left: 10px;" src={{devIcon}}>\n              <img *ngIf="isSelected(d)" width=\'20px\' style="margin-left: 10px;" src="assets/imgs/cheackright.png">\n                <ion-icon ios="ios-add-circle" md="md-add-circle" *ngIf="selectedArray.length > 0" style="color: white; font-size: 20px;"></ion-icon> -->\n           <!-- </ion-avatar>\n            <div>\n              <h2 style="color: gray">{{ d.Device_Name }}</h2>\n              <p style="color:gray;font-size: 10px;font-weight: 400;margin:0px;" ion-text text-wrap>\n                <span style="text-transform: uppercase; color: red;">{{ d.status }}\n                </span>\n                <span *ngIf="d.status_updated_at">{{ "since" | translate }}&nbsp;{{\n                  d.status_updated_at | date: "medium"\n                }}\n                </span>\n              </p>\n            </div>\n          </ion-item>\n        </div>\n      </div> -->\n\n    <div>\n        <p style="text-align: center; font-size: 2rem">\n            <b>{{\'Device Expired!\' | translate}}</b>\n        </p>\n        <p>\n            <button ion-button block outline color="secondary" (tap)="activate()">{{\'Activate\' | translate}}</button>\n        </p>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/apple/Desktop/white-labels/laiba/src/pages/live/expired/expired.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], ExpiredPage);
    return ExpiredPage;
}());

//# sourceMappingURL=expired.js.map

/***/ })

});
//# sourceMappingURL=41.js.map