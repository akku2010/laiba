webpackJsonp([27],{

/***/ 657:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TollPageModule", function() { return TollPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toll__ = __webpack_require__(679);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TollPageModule = /** @class */ (function () {
    function TollPageModule() {
    }
    TollPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__toll__["a" /* TollPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__toll__["a" /* TollPage */]),
            ],
        })
    ], TollPageModule);
    return TollPageModule;
}());

//# sourceMappingURL=toll.module.js.map

/***/ }),

/***/ 679:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TollPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TollPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TollPage = /** @class */ (function () {
    function TollPage(navCtrl, navParams, apicalldaywise) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalldaywise = apicalldaywise;
        this.toggled = false;
        this.toggled = false;
        this.tollList();
    }
    TollPage.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    TollPage.prototype.cancelSearch = function () {
        // this.toggle();
        this.toggled = false;
    };
    TollPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        var _baseURL;
        //_baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        _baseURL = "https://socket.oneqlik.in/toll/get?search=" + searchKey;
        this.apicalldaywise.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.list = data;
            //this.allDevices = data.devices;
            // console.log("fuel percentage: " + data.devices[0].fuel_percent)
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    TollPage.prototype.onClear = function (ev) {
        this.tollList();
        // this.getdevicesTemp();
        ev.target.value = '';
        // this.toggled = false;
    };
    TollPage.prototype.live = function () {
        this.navCtrl.push("TollMapPage");
    };
    TollPage.prototype.tollList = function () {
        var _this = this;
        console.log("inside", 123);
        var baseURLp = "https://socket.oneqlik.in/toll/get";
        this.apicalldaywise.startLoading().present();
        this.apicalldaywise.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalldaywise.stopLoading();
            console.log("toll list", data);
            _this.list = data;
        }, function (err) {
            _this.apicalldaywise.stopLoading();
            console.log(err);
        });
    };
    TollPage.prototype.addTolls = function () {
        this.navCtrl.push('TollAddTollPage');
    };
    TollPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TollPage');
    };
    TollPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-toll',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/laiba/src/pages/toll/toll.html"*/'<!--\n  Generated template for the TollPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Toll List</ion-title>\n    <!-- <ion-buttons end>\n      <button  ion-button icon-only>\n\n        <img src="assets/icon/stack-of-square-papers-svg.png">\n      </button>\n    </ion-buttons> -->\n\n    <ion-buttons end>\n      <button (click) = "live()" end>  Map View </button>\n      </ion-buttons>\n\n    <!-- <ion-buttons end>\n      <div>\n        <ion-icon name="radio-button-on"></ion-icon>\n        <ion-icon style="font-size: 2.2em;" color="light" *ngIf="!toggled" (click)="toggle()" name="search"></ion-icon>\n        <ion-searchbar *ngIf="toggled" (ionInput)="callSearch($event)" (ionClear)="onClear($event)"\n          (ionCancel)="cancelSearch($event)"></ion-searchbar>\n      </div>\n    </ion-buttons> -->\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content no-padding>\n<!-- <ion-buttons>\n<button (click) = "live()" end>  Map View </button>\n</ion-buttons> -->\n    <ion-card *ngFor="let item of list; let i = index">\n  <ion-item style="border-bottom: 2px solid #dedede;">\n\n    <ion-row>\n      <ion-col col-3>\n        <img src="assets/imgs/toll.png" style="\n          margin-top: -10px;\n          width: 40px;\n          height: 40px;" />\n      </ion-col>\n      <ion-col col-3>\n        <p style="\n          color:black;\n          font-size:16px;\n          margin: 0px;">\n          {{ item.tollName }}\n        </p>\n      </ion-col>\n      <ion-col col-6 style="text-align: end;">\n        <p style="\n          color:gray;\n          font-size:11px;\n          font-weight:400;\n         ">\n          {{item.tollTax}}\n        </p>\n\n        <p style="\n        font-size: 11px;\n        color:red;\n        font-weight:bold;\n\n        ">\n          {{\'Toll Tax\'}}\n        </p>\n      </ion-col>\n      <!-- <ion-col col-4>\n        <p style="\n          text-align: center;\n          font-size: 11px;\n          color:#11c1f3;\n          font-weight:bold;">\n          {{item.Date}}\n        </p>\n      </ion-col> -->\n\n    </ion-row>\n\n    <!-- <ion-row style="margin-top:2%;">\n      <ion-col col-4>\n        <p style="\n          color:gray;\n          font-size:11px;\n          font-weight:400;\n         ">\n          {{item.tollTax}}\n        </p>\n\n        <p style="\n        font-size: 11px;\n        color:red;\n        font-weight:bold;\n        ">\n          {{\'Toll Tax\'}}\n        </p>\n      </ion-col>\n\n      <ion-col col-4>\n        <p style="\n          color:gray;\n          font-size:11px;\n          font-weight:400;">\n          {{item.location.coordinates[\'0\']}}\n        </p>\n\n        <p style="\n        font-size: 11px;\n        color:#53ab53;\n        font-weight:bold;">\n          {{\'Latitude\' }}\n        </p>\n      </ion-col>\n      <ion-col col-4>\n        <p style="\n          color:gray;\n          font-size:11px;\n          font-weight:400;\n          margin-left: 8px;">\n          {{item.location.coordinates[\'1\']}}\n        </p>\n\n        <p style="\n        font-size: 11px;\n        color:red;\n        font-weight:bold;\n        margin-left: 8px;">\n          {{\'Longitude\'}}\n        </p>\n      </ion-col>\n    </ion-row> -->\n\n    <ion-row style="margin-top:2%;">\n\n      <ion-col col-4>\n        <p style="\n        color:gray;\n        font-size:11px;\n        font-weight:400;\n       ">\n          {{item.address}}\n        </p>\n\n        <p style="\n      color:#53ab53;\n      font-size: 11px;\n      font-weight: bold;\n      ">\n          {{\'Address\' }}\n        </p>\n      </ion-col>\n\n      <!-- <ion-col col-4>\n        <p style="\n          color:gray;\n          font-size:11px;\n          font-weight:400;">\n          {{item.location.coordinates[\'0\']}}\n        </p>\n\n        <p style="\n        font-size: 11px;\n        color:#53ab53;\n        font-weight:bold;">\n          {{\'Latitude\' }}\n        </p>\n      </ion-col>\n      <ion-col col-4>\n        <p style="\n          color:gray;\n          font-size:11px;\n          font-weight:400;\n          margin-left: 8px;">\n          {{item.location.coordinates[\'1\']}}\n        </p>\n\n        <p style="\n        font-size: 11px;\n        color:red;\n        font-weight:bold;\n        margin-left: 8px;">\n          {{\'Longitude\'}}\n        </p>\n      </ion-col> -->\n\n    </ion-row>\n\n\n    <!--\n    -->\n\n    <!-- </ion-scroll> -->\n\n\n  </ion-item>\n </ion-card>\n  <!-- <ion-fab right bottom>\n    <button ion-fab color="gpsc" (click)="addTolls()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab> -->\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/laiba/src/pages/toll/toll.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], TollPage);
    return TollPage;
}());

//# sourceMappingURL=toll.js.map

/***/ })

});
//# sourceMappingURL=27.js.map