webpackJsonp([25],{

/***/ 659:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TtPageModule", function() { return TtPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tt__ = __webpack_require__(776);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TtPageModule = /** @class */ (function () {
    function TtPageModule() {
    }
    TtPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__tt__["a" /* TtPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__tt__["a" /* TtPage */]),
            ],
        })
    ], TtPageModule);
    return TtPageModule;
}());

//# sourceMappingURL=tt.module.js.map

/***/ }),

/***/ 776:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TtPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(441);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_themeable_browser__ = __webpack_require__(777);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var iabRef = null;
var TtPage = /** @class */ (function () {
    function TtPage(navCtrl, navParams, apiservice, iab, alerCtrl, toastCtrl, themeableBrowser) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiservice = apiservice;
        this.iab = iab;
        this.alerCtrl = alerCtrl;
        this.toastCtrl = toastCtrl;
        this.themeableBrowser = themeableBrowser;
        this.iabRef = null;
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("islogin devices => " + JSON.stringify(this.islogin));
        this.payment();
    }
    TtPage.prototype.insertMyHeader = function () {
        iabRef.executeScript({
            code: "var b=document.querySelector('body'); var a=document.createElement('div');document.createTextNode('my own header!'); a.appendChild(newContent);b.parentNode.insertBefore(a,b);"
        }, function () {
            alert("header successfully added");
        });
    };
    TtPage.prototype.iabClose = function (event) {
        //iabRef.removeEventListener('loadstop', replaceHeaderImage);
        iabRef.removeEventListener('exit', this.iabClose);
    };
    TtPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TtPage');
    };
    TtPage.prototype.call = function () {
        //window.open(this.resp);
        if (this.resp == undefined) {
            var alerttemp = this.alerCtrl.create({
                message: "invalid-args",
                buttons: [{
                        text: 'Okay',
                    }]
            });
            alerttemp.present();
        }
        else {
            //   const options : InAppBrowserOptions = {
            //     zoom: 'no'
            //   }
            //   const browser = this.iab.create(this.resp,'_self',options);
            // //   // browser.insertCSS()
            //   browser.on('loadstop').subscribe(event => {
            //     browser.insertCSS({ code: "body{color: red;" });
            //  });
            var options = {
                statusbar: {
                    color: '#f54278'
                },
                toolbar: {
                    height: 50,
                    color: '#d80622'
                },
                title: {
                    color: '#ffffff',
                    showPageTitle: true
                },
                backButton: {
                    wwwImage: '/assets/icon/arrow.png',
                    imagePressed: 'back_pressed',
                    align: 'left',
                    // event: 'backPressed'
                    event: 'closePressed'
                },
                // closeButton: {
                //   image: 'close',
                //   imagePressed: 'close_pressed',
                //   align: 'right',
                //   event: 'closePressed'
                // },
                backButtonCanClose: true
            };
            var browser = this.themeableBrowser.create(this.resp, '_self', options);
            //  this.iab.create(this.resp, '_self', 'location=yes').insertCSS({ code: "body{color: red;" })
            //iabRef = this.iab.create(this.resp, '_blank', 'location=yes');
            //iabRef.addEventListener('loadstop', this.insertMyHeader);
            //iabRef.addEventListener('exit', this.iabClose);
        }
        //browser.on('')
    };
    TtPage.prototype.payment = function () {
        var _this = this;
        console.log("inside the payment");
        var url = "https://socket.oneqlik.in/pullData/getSetuLink?mobileNumber=" + this.islogin.phn;
        //var url = "https://socket.oneqlik.in/pullData/getSetuLink?mobileNumber=" + "9481773053";
        this.apiservice.startLoading().present();
        this.apiservice.getdevicesForAllVehiclesApi(url)
            .subscribe(function (resp) {
            _this.apiservice.stopLoading();
            console.log("server image url=> ", resp);
            console.log("chk response", resp);
            _this.resp = resp.link;
            console.log("final link", _this.resp);
            if (_this.resp == undefined) {
                var alerttemp = _this.alerCtrl.create({
                    message: "invalid-args",
                    buttons: [{
                            text: 'Okay',
                        }]
                });
                alerttemp.present();
            }
            //this.iab.create(this.resp, '_self' );
        }, function (err) {
            _this.apiservice.stopLoading();
        });
        // this.data = {
        //   "mobileNumber":"9481773053"
        // };
        this.apiservice.payment(this.data);
        // .subscribe(response => {
        //   console.log("chk response", response);
        //   this.resp = response.link
        //   console.log("final link", this.resp);
        //   //window.open(this.resp)
        // }
        // )
    };
    TtPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-tt',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/laiba/src/pages/tt/tt.html"*/'\n<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Payment</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-card>\n    <ion-card-header>\n      <img src="assets/imgs/download.png">\n    </ion-card-header>\n    <ion-card-content>\n      <button ion-button full color="gpsc" (click)="call()">Pay</button>\n    </ion-card-content>\n\n  </ion-card>\n<!-- <p (click)="call()">Test</p>\n<a href="resp">{{ resp }}</a> -->\n</ion-content>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/laiba/src/pages/tt/tt.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_themeable_browser__["a" /* ThemeableBrowser */]])
    ], TtPage);
    return TtPage;
}());

//# sourceMappingURL=tt.js.map

/***/ }),

/***/ 777:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export ThemeableBrowserObject */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ThemeableBrowser; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * @hidden
 */
var ThemeableBrowserObject = (function () {
    function ThemeableBrowserObject(url, target, styleOptions) {
        try {
            this._objectInstance = cordova.ThemeableBrowser.open(url, target, styleOptions);
        }
        catch (e) {
            window.open(url);
            console.warn('Native: ThemeableBrowser is not installed or you are running on a browser. Falling back to window.open.');
        }
    }
    /**
     * Displays an browser window that was opened hidden. Calling this has no effect
     * if the browser was already visible.
     */
    /**
       * Displays an browser window that was opened hidden. Calling this has no effect
       * if the browser was already visible.
       */
    ThemeableBrowserObject.prototype.show = /**
       * Displays an browser window that was opened hidden. Calling this has no effect
       * if the browser was already visible.
       */
    function () { };
    /**
     * Closes the browser window.
     */
    /**
       * Closes the browser window.
       */
    ThemeableBrowserObject.prototype.close = /**
       * Closes the browser window.
       */
    function () { };
    /**
     * Reloads the current page
     */
    /**
       * Reloads the current page
       */
    ThemeableBrowserObject.prototype.reload = /**
       * Reloads the current page
       */
    function () { };
    /**
     * Injects JavaScript code into the browser window.
     * @param script    Details of the script to run, specifying either a file or code key.
     * @returns {Promise<any>}
     */
    /**
       * Injects JavaScript code into the browser window.
       * @param script    Details of the script to run, specifying either a file or code key.
       * @returns {Promise<any>}
       */
    ThemeableBrowserObject.prototype.executeScript = /**
       * Injects JavaScript code into the browser window.
       * @param script    Details of the script to run, specifying either a file or code key.
       * @returns {Promise<any>}
       */
    function (script) { return; };
    /**
     * Injects CSS into the browser window.
     * @param css       Details of the script to run, specifying either a file or code key.
     * @returns {Promise<any>}
     */
    /**
       * Injects CSS into the browser window.
       * @param css       Details of the script to run, specifying either a file or code key.
       * @returns {Promise<any>}
       */
    ThemeableBrowserObject.prototype.insertCss = /**
       * Injects CSS into the browser window.
       * @param css       Details of the script to run, specifying either a file or code key.
       * @returns {Promise<any>}
       */
    function (css) { return; };
    /**
     * A method that allows you to listen to events happening in the browser.
     * Available events are: `ThemeableBrowserError`, `ThemeableBrowserWarning`, `critical`, `loadfail`, `unexpected`, `undefined`
     * @param event Event name
     * @returns {Observable<any>} Returns back an observable that will listen to the event on subscribe, and will stop listening to the event on unsubscribe.
     */
    /**
       * A method that allows you to listen to events happening in the browser.
       * Available events are: `ThemeableBrowserError`, `ThemeableBrowserWarning`, `critical`, `loadfail`, `unexpected`, `undefined`
       * @param event Event name
       * @returns {Observable<any>} Returns back an observable that will listen to the event on subscribe, and will stop listening to the event on unsubscribe.
       */
    ThemeableBrowserObject.prototype.on = /**
       * A method that allows you to listen to events happening in the browser.
       * Available events are: `ThemeableBrowserError`, `ThemeableBrowserWarning`, `critical`, `loadfail`, `unexpected`, `undefined`
       * @param event Event name
       * @returns {Observable<any>} Returns back an observable that will listen to the event on subscribe, and will stop listening to the event on unsubscribe.
       */
    function (event) {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"](function (observer) {
            _this._objectInstance.addEventListener(event, observer.next.bind(observer));
            return function () { return _this._objectInstance.removeEventListener(event, observer.next.bind(observer)); };
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["c" /* CordovaInstance */])({ sync: true }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ThemeableBrowserObject.prototype, "show", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["c" /* CordovaInstance */])({ sync: true }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ThemeableBrowserObject.prototype, "close", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["c" /* CordovaInstance */])({ sync: true }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ThemeableBrowserObject.prototype, "reload", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["c" /* CordovaInstance */])(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], ThemeableBrowserObject.prototype, "executeScript", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["c" /* CordovaInstance */])(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", Promise)
    ], ThemeableBrowserObject.prototype, "insertCss", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["e" /* InstanceCheck */])({ observable: true }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"])
    ], ThemeableBrowserObject.prototype, "on", null);
    return ThemeableBrowserObject;
}());

/**
 * @name Themeable Browser
 * @description
 * In-app browser that allows styling.
 *
 * @usage
 * ```typescript
 * import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
 *
 * constructor(private themeableBrowser: ThemeableBrowser) { }
 *
 * ...
 *
 * // can add options from the original InAppBrowser in a JavaScript object form (not string)
 * // This options object also takes additional parameters introduced by the ThemeableBrowser plugin
 * // This example only shows the additional parameters for ThemeableBrowser
 * // Note that that `image` and `imagePressed` values refer to resources that are stored in your app
 * const options: ThemeableBrowserOptions = {
 *      statusbar: {
 *          color: '#ffffffff'
 *      },
 *      toolbar: {
 *          height: 44,
 *          color: '#f0f0f0ff'
 *      },
 *      title: {
 *          color: '#003264ff',
 *          showPageTitle: true
 *      },
 *      backButton: {
 *          image: 'back',
 *          imagePressed: 'back_pressed',
 *          align: 'left',
 *          event: 'backPressed'
 *      },
 *      forwardButton: {
 *          image: 'forward',
 *          imagePressed: 'forward_pressed',
 *          align: 'left',
 *          event: 'forwardPressed'
 *      },
 *      closeButton: {
 *          image: 'close',
 *          imagePressed: 'close_pressed',
 *          align: 'left',
 *          event: 'closePressed'
 *      },
 *      customButtons: [
 *          {
 *              image: 'share',
 *              imagePressed: 'share_pressed',
 *              align: 'right',
 *              event: 'sharePressed'
 *          }
 *      ],
 *      menu: {
 *          image: 'menu',
 *          imagePressed: 'menu_pressed',
 *          title: 'Test',
 *          cancel: 'Cancel',
 *          align: 'right',
 *          items: [
 *              {
 *                  event: 'helloPressed',
 *                  label: 'Hello World!'
 *              },
 *              {
 *                  event: 'testPressed',
 *                  label: 'Test!'
 *              }
 *          ]
 *      },
 *      backButtonCanClose: true
 * };
 *
 * const browser: ThemeableBrowserObject = this.themeableBrowser.create('https://ionic.io', '_blank', options);
 *
 * ```
 * We suggest that you refer to the plugin's repository for additional information on usage that may not be covered here.
 * @classes
 * ThemeableBrowserObject
 * @interfaces
 * ThemeableBrowserButton
 * ThemeableBrowserOptions
 */
var ThemeableBrowser = (function (_super) {
    __extends(ThemeableBrowser, _super);
    function ThemeableBrowser() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * Creates a browser instance
     * @param url {string} URL to open
     * @param target {string} Target
     * @param styleOptions {ThemeableBrowserOptions} Themeable browser options
     * @returns {ThemeableBrowserObject}
     */
    /**
       * Creates a browser instance
       * @param url {string} URL to open
       * @param target {string} Target
       * @param styleOptions {ThemeableBrowserOptions} Themeable browser options
       * @returns {ThemeableBrowserObject}
       */
    ThemeableBrowser.prototype.create = /**
       * Creates a browser instance
       * @param url {string} URL to open
       * @param target {string} Target
       * @param styleOptions {ThemeableBrowserOptions} Themeable browser options
       * @returns {ThemeableBrowserObject}
       */
    function (url, target, styleOptions) {
        return new ThemeableBrowserObject(url, target, styleOptions);
    };
    ThemeableBrowser.decorators = [
        { type: __WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"] },
    ];
    /**
     * @name Themeable Browser
     * @description
     * In-app browser that allows styling.
     *
     * @usage
     * ```typescript
     * import { ThemeableBrowser, ThemeableBrowserOptions, ThemeableBrowserObject } from '@ionic-native/themeable-browser';
     *
     * constructor(private themeableBrowser: ThemeableBrowser) { }
     *
     * ...
     *
     * // can add options from the original InAppBrowser in a JavaScript object form (not string)
     * // This options object also takes additional parameters introduced by the ThemeableBrowser plugin
     * // This example only shows the additional parameters for ThemeableBrowser
     * // Note that that `image` and `imagePressed` values refer to resources that are stored in your app
     * const options: ThemeableBrowserOptions = {
     *      statusbar: {
     *          color: '#ffffffff'
     *      },
     *      toolbar: {
     *          height: 44,
     *          color: '#f0f0f0ff'
     *      },
     *      title: {
     *          color: '#003264ff',
     *          showPageTitle: true
     *      },
     *      backButton: {
     *          image: 'back',
     *          imagePressed: 'back_pressed',
     *          align: 'left',
     *          event: 'backPressed'
     *      },
     *      forwardButton: {
     *          image: 'forward',
     *          imagePressed: 'forward_pressed',
     *          align: 'left',
     *          event: 'forwardPressed'
     *      },
     *      closeButton: {
     *          image: 'close',
     *          imagePressed: 'close_pressed',
     *          align: 'left',
     *          event: 'closePressed'
     *      },
     *      customButtons: [
     *          {
     *              image: 'share',
     *              imagePressed: 'share_pressed',
     *              align: 'right',
     *              event: 'sharePressed'
     *          }
     *      ],
     *      menu: {
     *          image: 'menu',
     *          imagePressed: 'menu_pressed',
     *          title: 'Test',
     *          cancel: 'Cancel',
     *          align: 'right',
     *          items: [
     *              {
     *                  event: 'helloPressed',
     *                  label: 'Hello World!'
     *              },
     *              {
     *                  event: 'testPressed',
     *                  label: 'Test!'
     *              }
     *          ]
     *      },
     *      backButtonCanClose: true
     * };
     *
     * const browser: ThemeableBrowserObject = this.themeableBrowser.create('https://ionic.io', '_blank', options);
     *
     * ```
     * We suggest that you refer to the plugin's repository for additional information on usage that may not be covered here.
     * @classes
     * ThemeableBrowserObject
     * @interfaces
     * ThemeableBrowserButton
     * ThemeableBrowserOptions
     */
    ThemeableBrowser = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["h" /* Plugin */])({
            pluginName: 'ThemeableBrowser',
            plugin: 'cordova-plugin-themeablebrowser',
            pluginRef: 'cordova.ThemeableBrowser',
            repo: 'https://github.com/initialxy/cordova-plugin-themeablebrowser',
            platforms: ['Amazon Fire OS', 'Android', 'Blackberry 10', 'Browser', 'FirefoxOS', 'iOS', 'Ubuntu', 'Windows', 'Windows Phone']
        })
    ], ThemeableBrowser);
    return ThemeableBrowser;
}(__WEBPACK_IMPORTED_MODULE_1__ionic_native_core__["g" /* IonicNativePlugin */]));

//# sourceMappingURL=index.js.map

/***/ })

});
//# sourceMappingURL=25.js.map